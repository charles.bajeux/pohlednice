from pohlednice import U8, U16, U32, I8, I16, I32, F32, F64, Option, Bool, ByteArray, String, Tuple, List, Struct, Enum, Dict

print(list(U8.to_bytes(123)))
print(list(U16.to_bytes(1233)))
print(list(U32.to_bytes(123456)))


print(45, list(hex(b) for b in I8.to_bytes(45)))
print(45, I8.from_bytes(I8.to_bytes(45)))
print(-45, list(hex(b) for b in I8.to_bytes(-45)))
print(-45, I8.from_bytes(I8.to_bytes(-45)))
print(0, list(hex(b) for b in I16.to_bytes(0)))
print(0, I16.from_bytes(I16.to_bytes(0)))
print(1, list(hex(b) for b in I16.to_bytes(1)))
print(1, I16.from_bytes(I16.to_bytes(1)))
print(-1, list(hex(b) for b in I16.to_bytes(-1)))
print(-1, I16.from_bytes(I16.to_bytes(-1)))
print(63, list(hex(b) for b in I16.to_bytes(63)))
print(63, I16.from_bytes(I16.to_bytes(63)))
print(64, list(hex(b) for b in I16.to_bytes(64)))
print(64, I16.from_bytes(I16.to_bytes(64)))
print(-64, list(hex(b) for b in I16.to_bytes(-64)))
print(-64, I16.from_bytes(I16.to_bytes(-64)))
print(123456, list(hex(b) for b in I32.to_bytes(123456)))
print(123456, I32.from_bytes(I32.to_bytes(123456)))
print(-123456, list(hex(b) for b in I32.to_bytes(-123456)))
print(-123456, I32.from_bytes(I32.to_bytes(-123456)))

print(list(hex(x) for x in F32.to_bytes(-32.005859375)))
print(list(hex(x) for x in F64.to_bytes(-32.005859375)))

U8U16 = Tuple[U8, Option[Bool], U32]
    
print(U8U16.from_bytes(U8U16.to_bytes((123, None, 123456))))
print(ByteArray.from_bytes(ByteArray.to_bytes(b'1,2,3,4')))
print(ByteArray.from_bytes(ByteArray.to_bytes((1, 2, 3, 4))))

U32s = List[U32]

print(U32s.from_bytes(U32s.to_bytes((564, 123456, 123))))

class ABC(Struct):
    a: U16
    b: String
    c: Option[ByteArray]

print(ABC.from_bytes(ABC.to_bytes(ABC(40, 'Trollě', None))))

class Color(Enum):
    Red: None
    Blue: None
    Green: None
    Custom: Tuple[U8, U8, U8]

print(Color.Red, Color.Blue, Color.Custom)
print(Color.from_bytes(Color.to_bytes(Color.Custom((0xff, 0xf0, 0x0f)))))

StringToU16 = Dict[String, U16]

print(StringToU16.from_bytes(StringToU16.to_bytes({'Cuicui':0x55, 'Coincoin':12244})))
